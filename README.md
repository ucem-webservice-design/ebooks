Ebooks y documentos
===================

Colección de libros y documentos sobre el diseño e implementación de servicios
web, y demás tecnologías relevantes.

#### Spring
* [Spring Boot In Action](https://gitlab.com/ucem-webservice-design/ebooks/raw/27a42f81b685e9ad4236bc4de763be32896e13b6/SpringBootInAction.pdf) - Craig Walls
* [Spring Security 3.1](https://gitlab.com/ucem-webservice-design/ebooks/raw/27a42f81b685e9ad4236bc4de763be32896e13b6/SpringSecurity3.1.pdf) - Robert Winch & Peter Mularien
* [Pro Spring Boot](https://gitlab.com/ucem-webservice-design/ebooks/raw/1f928858633de6aed497d9396f8cedd11c885741/ProSpringBoot.pdf) - Felipe Gutierrez

#### REST
* [Learn REST: A RESTful Tutorial](http://www.restapitutorial.com/) - Todd Fredrich, REST API Expert
* [White House Web API Standards](https://github.com/WhiteHouse/api-standards) - The White House, Washington, D.C.
* [REFCARDZ RESTful Architecture](https://gitlab.com/ucem-webservice-design/ebooks/raw/1f928858633de6aed497d9396f8cedd11c885741/RefcardzRestfulArchitectureupdated.pdf) - Brian Sletten

### JSON
* [REFCARDZ Core JSON](https://gitlab.com/ucem-webservice-design/ebooks/raw/1f928858633de6aed497d9396f8cedd11c885741/RefcardzCoreJSON.pdf) - Tom Marrs

### JAX-RS
* [RESTful Java with JAX-RS 2.0](https://gitlab.com/ucem-webservice-design/ebooks/raw/master/RestfulJavaWithJAX-RS-2-0.pdf) - Bill Burke

### Maven
* [Maven in 5 minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) - Apache Maven Project

### Patrones
* [RESTful Java Patterns and Best Practices](https://gitlab.com/ucem-webservice-design/ebooks/raw/0d1b6b9066f106504873e00d15dc99c76798acbb/RESTfulJavaPatternsAndBestPractices.pdf) - Bhakti Mehta

### Blogs y sitios web informativos
* [Diferencia entre URLs y URIs] (https://danielmiessler.com/study/url-uri/#gs.ia1pP_U) - Daniel Miessler
* [Artículos varios sobre el desarrollo de APIs] (http://api-university.com/blog/) API-University
